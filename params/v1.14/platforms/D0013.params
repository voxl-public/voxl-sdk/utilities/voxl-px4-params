################################################################################
# Hardware-Specific Configuration for ModalAI D0013
# This is NOT a complete set of PX4 parameters. These are non-default parameters
# specific to the airframe and are meant to be loaded over the defaults.
#
# work in progress
################################################################################



################################################################################
# quadcopter X type, voxl2 mini mounted upside down
################################################################################
1	1	SYS_AUTOSTART	4001	6
1	1	MAV_TYPE	2	6
1	1	SENS_BOARD_ROT	8	6


# expected hover current, used to initialize the battery average current filter
# for flight time remaining estimate, so we use current at takeoff with full battery.
1	1	BAT_AVRG_CURRENT	11.0	9




################################################################################
# Motors/ESCs
################################################################################

# motor specific config
1	1	MPC_THR_HOVER	0.25	9
1	1	THR_MDL_FAC	0.9	9
1	1	VOXL_ESC_RPM_MAX	25000	6
1	1	VOXL_ESC_RPM_MIN	3000	6

## 80% max throttle is probably okay for a lipo,
## li-ion probably can't keep up
1	1	MPC_THR_MAX	0.80	9
1	1	MPC_THR_MIN	0.08	9
1	1	MPC_MANTHR_MIN	0.08	9

# motor mapping
# FUNC1-4 refers to ESC channels 0-3
# 101-104 refers to the px4 prop mapping
1	1	VOXL_ESC_FUNC4	101	6
1	1	VOXL_ESC_FUNC2	102	6
1	1	VOXL_ESC_FUNC1	103	6
1	1	VOXL_ESC_FUNC3	104	6


# set all directions to "forward" the correct motor spins directions should
# be set by ESC firmware and correct/consistent wiring
1	1	VOXL_ESC_SDIR1	0	6
1	1	VOXL_ESC_SDIR2	0	6
1	1	VOXL_ESC_SDIR3	0	6
1	1	VOXL_ESC_SDIR4	0	6

# MISC ModalIO common setup
1	1	VOXL_ESC_BAUD	2000000	6
1	1	VOXL_ESC_CONFIG	1	6
1	1	VOXL_ESC_MODE	1	6
1	1	VOXL_ESC_REV	0	6
1	1	VOXL_ESC_T_COSP	0.990000009536743164	9
1	1	VOXL_ESC_T_DEAD	20	6
1	1	VOXL_ESC_T_EXPO	35	6
1	1	VOXL_ESC_T_MINF	0.150000005960464478	9
1	1	VOXL_ESC_T_PERC	90	6
1	1	VOXL_ESC_VLOG	1	6

# disable bat scaling with our own RPM controlled ESCs!
1	1	MC_BAT_SCALE_EN	0	6



################################################################################
# Geometry (voxl-vision-hub provides EV VIO data at COG so EV_POS is 0
################################################################################
1	1	EKF2_IMU_POS_X	0.010	9
1	1	EKF2_IMU_POS_Y	0.001	9
1	1	EKF2_IMU_POS_Z	0.016	9
1	1	EKF2_EV_POS_X	0.0	9
1	1	EKF2_EV_POS_Y	0.0	9
1	1	EKF2_EV_POS_Z	0.0	9


###############################################################################
# Rotor locations for mixing
# TODO update this for new frame, not critical though
###############################################################################

1	1	CA_AIRFRAME	0	6
1	1	CA_ROTOR_COUNT	4	6
1	1	CA_ROTOR0_PX	0.085	9
1	1	CA_ROTOR0_PY	0.0625	9
1	1	CA_ROTOR1_PX	-0.085	9
1	1	CA_ROTOR1_PY	-0.0625	9
1	1	CA_ROTOR2_PX	0.085	9
1	1	CA_ROTOR2_PY	-0.0625	9
1	1	CA_ROTOR3_PX	-0.085	9
1	1	CA_ROTOR3_PY	0.0625	9
1	1	CA_ROTOR0_AX	0.0	9
1	1	CA_ROTOR0_AY	0.0	9
1	1	CA_ROTOR1_AX	0.0	9
1	1	CA_ROTOR1_AY	0.0	9
1	1	CA_ROTOR2_AX	0.0	9
1	1	CA_ROTOR2_AY	0.0	9
1	1	CA_ROTOR3_AX	0.0	9
1	1	CA_ROTOR3_AY	0.0	9
1	1	CA_ROTOR0_KM	0.05	9
1	1	CA_ROTOR1_KM	0.05	9
1	1	CA_ROTOR2_KM	-0.05	9
1	1	CA_ROTOR3_KM	-0.05	9

################################################################################
# Noise management
################################################################################

1	1	MOT_SLEW_MAX	0.0	9
1	1	IMU_DGYRO_CUTOFF	20.0	9
1	1	IMU_ACCEL_CUTOFF	30.0	9
1	1	IMU_GYRO_CUTOFF	180.0	9

# dynamic notch filter
1	1	IMU_GYRO_DNF_BW	20.000000000000000000	9
1	1	IMU_GYRO_DNF_EN	2	6
1	1	IMU_GYRO_DNF_HMC	3	6
1	1	IMU_GYRO_FFT_EN	1	6
1	1	IMU_GYRO_FFT_LEN	1024	6
1	1	IMU_GYRO_FFT_MAX	192.000000000000000000	9
1	1	IMU_GYRO_FFT_MIN	32.000000000000000000	9
1	1	IMU_GYRO_FFT_SNR	10.000000000000000000	9


################################################################################
# Attitude PID
################################################################################

# disable airmode, it make the system unpredictable when touching the ground
1	1	MC_AIRMODE	0	6

# Common
1	1	MC_ROLLRATE_K	1.0	9
1	1	MC_PITCHRATE_K	1.0	9
1	1	MC_YAWRATE_K	1.0	9
1	1	MC_ROLL_CUTOFF	30.0	9
1	1	MC_PITCH_CUTOFF	30.0	9
1	1	MC_YAW_CUTOFF	10.0	9

# Roll PID
1	1	MC_ROLL_P	16.0	9
1	1	MC_ROLLRATE_P	0.069	9
1	1	MC_ROLLRATE_I	0.253	9
1	1	MC_ROLLRATE_D	0.000	9

# Pitch PID
1	1	MC_PITCH_P	16.0	9
1	1	MC_PITCHRATE_P	0.11	9
1	1	MC_PITCHRATE_I	0.40	9
1	1	MC_PITCHRATE_D	0.0004	9

# Yaw PID
1	1	MC_YAW_P	2.8	9
1	1	MC_YAWRATE_P	0.06	9
1	1	MC_YAWRATE_I	0.5	9
1	1	MC_YAWRATE_D	0.0	9

# drop max rates down from default of 220 so VIO camera is happier
# for reference, the voxl-vision-hub figure 8 peaks at 135 degrees/s yaw max
1	1	MC_ROLLRATE_MAX	130.0	9
1	1	MC_PITCHRATE_MAX	130.0	9
1	1	MC_YAWRATE_MAX	150.0	9

# autotune settings
# 1	1	MC_AT_AXES	1	6
# 1	1	MC_AT_RISE_TIME	0.05	9
# 1	1	MC_AT_SYSID_AMP	4.0	9



###############################################################################
# takeoff and land params
#
# spoolup and ramp only help in position mode
###############################################################################

# smooth takeoff
1	1	MPC_TKO_RAMP_T	1.00	9
1	1	MPC_TKO_SPEED	1.50	9
1	1	COM_SPOOLUP_TIME	2.0	9

# increase rotation check a bit to stop it think it's taken off when arming
1	1	LNDMC_ROT_MAX	30.0	9

## speed up auto disarm on the ground
1	1	LNDMC_TRIG_TIME	0.5	9
1	1	COM_DISARM_LAND	0.1	9

# seconds to auto-disarm if armed but not taken off yet
1	1	COM_DISARM_PRFLT	20	9

# allow arming upside down
1	1	COM_ARM_BAD_INOV	1	6
1	1	COM_ARM_EKF_POS	0.000000000000000000	9
1	1	COM_ARM_EKF_VEL	0.000000000000000000	9
1	1	COM_ARM_EKF_YAW	0.000000000000000000	9
1	1	COM_ARM_IMU_ACC	0.000000000000000000	9
1	1	COM_ARM_IMU_GYR	0.000000000000000000	9
1	1	COM_ARM_EKF_BIAS	0.0	9
1	1	COM_ARM_EKF_HGT	0.0	9
1	1	COM_ARM_MAG_ANG	-1	6

1	1	FD_FAIL_P	180	6
1	1	FD_FAIL_R	180	6